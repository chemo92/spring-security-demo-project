package com.progcraft.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class MainController {

  @RequestMapping(value = "/")
  public String home(Map<String, Object> model) {
    model.put("message", "Hello World");
    return "home";
  }

  @RequestMapping(value = "/secured")
  public String secured() {
    return "secured";
  }
}
